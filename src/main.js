import Vue from 'vue'
import App from './App.vue'
import echarts from 'echarts'
import axios from "axios";
import VueAxios from "vue-axios";


Vue.prototype.$echarts = echarts
Vue.use(VueAxios,axios)

//console中不提示“正在使用开发版”
Vue.config.productionTip = false 

new Vue({
  render: h => h(App),
}).$mount('#app')
